package com.example.roompersistence;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.roompersistence.dao.StudentDao;
import com.example.roompersistence.databinding.ActivityMainBinding;
import com.example.roompersistence.model.Students;
import com.example.roompersistence.database.StudentDatabase;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private final int PERMISSION_CODE = 1;
    private final String PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    ActivityMainBinding binding;
    private StudentDao studentDao;

    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        StudentDatabase studentDatabase = StudentDatabase.getInstance(this);
        studentDao = studentDatabase.getDao();
        binding.txtShowAllStudent.setOnClickListener(v -> {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            List<Students> list = studentDao.getAllStudent();
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item);
            for (int i = 0; i < list.size(); i++) {
                arrayAdapter.add(list.get(i).getName());
            }
            builder.setTitle("All data in table")
                    .setAdapter(arrayAdapter, (dialog, which) -> {})
                    .create().show();
        });

        binding.imageView.setOnClickListener(v -> {
            mPermissionResultLauncher.launch(PERMISSION_READ_EXTERNAL_STORAGE);
        });

        binding.btnAddStudent.setOnClickListener(v -> {
            String name = binding.editName.getText().toString();
            String pass = binding.editPassword.getText().toString();
            Students students = new Students(0, name, pass, url);
            studentDao.insert(students);
            Toast.makeText(this, "Successfully", Toast.LENGTH_SHORT).show();
        });
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        ActivityCompat.startActivityForResult(MainActivity.this, intent, PERMISSION_CODE, new Bundle());
    }

    private void permissionGranted() {
        boolean isReadPermissionGranted = ActivityCompat.checkSelfPermission(this, PERMISSION_READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;
        if (isReadPermissionGranted) {
            openGallery();
        }
    }

    private final ActivityResultLauncher<String> mPermissionResultLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            result -> {
                if (result) {
                    permissionGranted();
                } else {
                    Toast.makeText(MainActivity.this, "Permission denied...", Toast.LENGTH_SHORT).show();
                }
            }
    );

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PERMISSION_CODE && data != null) {
            Uri selected = data.getData();
            String[] filePathColunm = {MediaStore.Images.Media.DATA};
            if (selected != null) {
                Cursor cursor = getContentResolver().query(selected, filePathColunm, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    int colunmIndex = cursor.getColumnIndex(filePathColunm[0]);
                    String photoPath = cursor.getString(colunmIndex);
                    url = selected.toString();
                    Log.d(TAG, "onActivityResult: " + url);
                    binding.imageView.setImageBitmap(BitmapFactory.decodeFile(photoPath));
                    cursor.close();
                }
            }

        }

    }

}