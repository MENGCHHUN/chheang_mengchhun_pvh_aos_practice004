package com.example.roompersistence.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.roompersistence.dao.StudentDao;
import com.example.roompersistence.model.Students;

@Database(entities = {Students.class}, version = 2)
public abstract class StudentDatabase extends RoomDatabase {
    public abstract StudentDao getDao();
    public static  StudentDatabase INSTANCE;
    public  static  StudentDatabase getInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context, StudentDatabase.class, "StudentDatabase")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
