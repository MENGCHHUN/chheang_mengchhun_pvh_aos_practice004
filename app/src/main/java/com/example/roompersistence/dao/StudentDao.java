package com.example.roompersistence.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.roompersistence.model.Students;

import java.util.List;

@Dao
public interface StudentDao {
    @Insert
    void insert(Students students);
    @Update
    void update(Students students);

    @Query("SELECT * FROM students")
    List<Students> getAllStudent();

}
